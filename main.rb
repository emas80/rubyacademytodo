require 'rubygems'
require 'bundler/setup'
require 'sinatra'
require 'data_mapper'
require 'json'

DataMapper.setup(:default, ENV['DATABASE_URL'] || "sqlite3://#{Dir.pwd}/development.db")

class Task
  include DataMapper::Resource
  property :id,           Serial
  property :name,         String
  property :completed_at, DateTime
end


get '/' do
    @tasks = Task.all
  erb :index
end

get '/task/new' do
  erb :new
end

get '/task/:id' do
  @task = Task.get(params[:id])
  erb :task
end

get '/task/:id/delete' do
  @task = Task.get(params[:id])
  erb :delete
end

delete '/task/:id' do
  Task.get(params[:id]).destroy
  redirect "/"
end

put '/task/update' do
  task = Task.get(params[:id])
  task.name = params[:name]
  task.completed_at = params[:completed] ?  Time.now : nil
  if task.save
    status 201
    redirect "/"
  else
    status 412
    redirect '/'
  end
end

post '/task/create' do
  task = Task.new(:name => params[:name])
  if task.save
    status 201
    redirect "/"
  else
    status 412
    redirect '/'
  end
end

get '/provaJson' do
  content_type :json
  { :key1 => 'value1', :key2 => 'value2' }.to_json
end


DataMapper.auto_upgrade!
 